
$(document).ready(function () {
    function nextImage() {
        if ($("img.active").hasClass("last")) {
            $("img.active").removeClass("active");
            $("img.first").addClass("active");
        } else {
            var imgAttiva = $("img.active");
            var prossimaImg = $("img.active").next();
        }

        imgAttiva.removeClass("active");
        prossimaImg.addClass("active");
    }

    var loop = setInterval(nextImage, 5000);
});